import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Login from './components/Login';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <Router>
        <div>
          {console.log('salom')}
          <Route exact path='/' component={App} />
          <Route path="/login" component={Login} />
        </div>
    </Router>,
    document.getElementById('root'));
registerServiceWorker();